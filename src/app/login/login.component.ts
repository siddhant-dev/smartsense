import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {Router } from '@angular/router';
import { AllAPIService } from '../servies/all-api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  errMsg: string;
  errMsgPass: string;
  msg: string;
  hide = true;

  constructor(public fb: FormBuilder, private authService: AllAPIService,
              private router: Router) { }

  ngOnInit() {

    this.loginForm = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email
        ]
      ],
      password: ['', [
        Validators.required,
        Validators.minLength(6)
        ]
      ],
    });
  }

  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password');
}

  async login() {

    this.msg = null;
    const data =  {email: this.email.value, password: this.password.value};

    this.authService.auth(data).subscribe((payload: any) => {
      if(payload.status === 200){
        this.router.navigate(['/']);
      }

    });
  }



}
