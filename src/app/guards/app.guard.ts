import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AllAPIService } from '../servies/all-api.service';

@Injectable({
  providedIn: 'root'
})
export class AppGuard implements CanActivate {

  constructor( private auth: AllAPIService , private route: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (localStorage.getItem('user')) {
      
      return true;
    } else {
      this.route.navigate(['/login']);
      return false;
    }
  }

}
