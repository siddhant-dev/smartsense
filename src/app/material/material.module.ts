import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule, MatCardModule, MatButtonModule, MatProgressSpinnerModule,
  MatStepperModule, MatSelectModule, MatIconModule, MatProgressBarModule } from '@angular/material';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatInputModule, MatCardModule, MatButtonModule, MatProgressSpinnerModule,
  MatStepperModule, MatSelectModule, MatIconModule, MatProgressBarModule
  ],
  exports: [
    CommonModule,
    MatInputModule, MatCardModule, MatButtonModule, MatProgressSpinnerModule,
  MatStepperModule, MatSelectModule, MatIconModule, MatProgressBarModule
  ]
})
export class MaterialModule { }
