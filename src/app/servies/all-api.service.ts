import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {map, first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AllAPIService {

  url = 'http://gifkar.cloudapp.net:8083/v1.0';
  token: string;
  user$: any;

  constructor(public http: HttpClient) { }

  auth(body: any): Observable<any> {
    return this.http.post(`${this.url}/login`, body).pipe(
      map((data: any) => {
        if (data.status === 200) {
          this.token = data.payload.token;
          localStorage.setItem('user', data.payload.token);
          this.user$ = data.payload.data.user;
          return data;
        }
      })
    );

  }

  signOut(): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        Authorization : localStorage.getItem('user').toString()
      })};
      console.log(httpOptions);
    return this.http.get(`${this.url}/logout`, httpOptions).pipe(first());
  }

  getToken(): string {
    return this.token;
  }
}
