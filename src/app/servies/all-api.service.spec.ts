import { TestBed } from '@angular/core/testing';

import { AllAPIService } from './all-api.service';

describe('AllAPIService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AllAPIService = TestBed.get(AllAPIService);
    expect(service).toBeTruthy();
  });
});
