import { Component, OnInit } from '@angular/core';
import { AllAPIService } from './servies/all-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'SmartSenseHR';
  loggedIn: boolean;

  constructor(private auth: AllAPIService) {}

  ngOnInit(){
   if(localStorage.getItem('user')) {
     this.loggedIn = true;
  }

 
}
signOut(){
  this.auth.signOut().subscribe((payload: any) => {
    console.log(payload);
  })
}
}
